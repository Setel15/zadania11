//założenia:
//hasło pochodzi z bazy danych i przeszło walidację podczas tworzenia konta
//jednym z wymagań dotyczących hasła była minimalna długość 8 znaków
import React from "react";

const PasswordInput = ({ password, onSuccess }) => {
  //initialize helper and variables
  const arrayNotInclude = (arr, val) => {
    return arr.findIndex((elem) => elem === val) === -1;
  };

  const numberOfInputs = password.length + Math.ceil(Math.random() * 5) + 1;
  const numberOfActiveInputs = Math.ceil(Math.random() * (password.length / 2)) + 3;
  let positionsForActiveInputs = [];
  let inputs = [];
  let activeInputs = [];
  let inputsValue = [];

  const checkCorrect = (e, i) => {
    inputsValue[i] = e.target.value || false;
    if (arrayNotInclude(inputsValue, false)) {
      if (activeInputs.every((num) => password[num] === inputsValue[num])) {
        onSuccess();
      }
    }
  };
  //get positions of active inputs and sort ascending
  for (let i = 0; i < numberOfActiveInputs; i++) {
    const randomPosition = Math.floor(Math.random() * (password.length + 1));
    if (arrayNotInclude(positionsForActiveInputs, randomPosition)) {
      positionsForActiveInputs.push(randomPosition);
    } else {
      i--;
    }
  }
  positionsForActiveInputs.sort((a, b) => a - b);
  //generate active and disables inputs
  for (let i = 0; i < numberOfInputs; i++) {
    if (positionsForActiveInputs[0] === i) {
      positionsForActiveInputs.shift();
      inputs.push(
        <input
          type="password"
          maxLength="1"
          key={i}
          onChange={(e) => checkCorrect(e, i)}
          autoFocus={inputs.length === 0}
        />
      );
      inputsValue.push(false);
      activeInputs.push(i);
    } else {
      inputs.push(<input type="password" disabled key={i} />);
      inputsValue.push(null);
    }
  }

  return <div className="inputs">{inputs}</div>;
};
export default PasswordInput;
