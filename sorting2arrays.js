//initialize Arrays
let A = [];
let B = [];
for (let i = 0; i < 10000; i++) {
  if (i < 40) {
    A.push(Math.ceil(Math.random() * 50));
  }
  B.push(Math.ceil(Math.random() * 50));
}
//initialize helpers
const arrayToObject = (arr) => {
  arr.sort();
  let arrAsObject = {};
  let current = arr[0];
  let counter = 1;
  for (let num of arr) {
    if (num === current) {
      counter++;
    } else {
      arrAsObject[current] = counter;
      current = num;
      counter = 1;
    }
  }
  return arrAsObject;
};
Array.prototype.uniqueValues = function () {
  return [...new Set(this)];
};
//alghoritms
const onlyPrimes = (...arrays) => {
  let C = [];
  for (const array of arrays) {
    for (const num of array) {
      let prime = true;
      for (let i = 2; i <= num / 2; i++) {
        if (num % i == 0) {
          prime = false;
        }
      }
      if (prime) {
        C.push(num);
      }
    }
  }
  return C.uniqueValues();
};

const mostPopular = (arr = B, count = 13) => {
  const arrAsObject = arrayToObject(arr);
  return Object.keys(arrAsObject)
    .sort((a, b) => arrAsObject[b] - arrAsObject[a])
    .splice(0, count)
    .map((numberAsString) => parseInt(numberAsString));
};

const missing = (arrA = A, arrB = B) => {
  let C = [];
  const arrBAsObject = arrayToObject(arrB);
  for (let i = 1; i <= 50; i++) {
    if (arrA.findIndex((elem) => elem === val) === -1) {
      C.push((new Object()[i] = arrBAsObject[i] || 0));
    }
  }
  return C;
};

const lessPopularDivisbile = (arr = B, count = 10, divisible = 3) => {
  const arrAsObject = arrayToObject(arr.filter((num) => num % divisible === 0));
  return Object.keys(arrAsObject)
    .sort((a, b) => arrAsObject[a] - arrAsObject[b])
    .splice(0, count)
    .map((numberAsString) => parseInt(numberAsString));
};

const arraysToMixedObject = (arrA = A, arrB = B) => {
  const uniqueA = A.uniqueValues();
  const arrAsObject = arrayToObject(arrB);
  const C = {};
  for (let i = 0; i < uniqueA.length; i++) {
    C[uniqueA[i]] = arrAsObject[uniqueA[i]] || 0;
  }
  return C;
};
// console.log("-1: " + B.findIndex((elem) => elem === -1));
// console.log("0: " + B.findIndex((elem) => elem === 0));
// console.log("1: " + B.findIndex((elem) => elem === 1));
// console.log("49: " + B.findIndex((elem) => elem === 49));
// console.log("50: " + B.findIndex((elem) => elem === 50));
// console.log("51: " + B.findIndex((elem) => elem === 51));
